import { employeesData } from "./data.js"
const baseImgUrl = "./img/"
const box = document.querySelector('.section')
const searchBtn = document.querySelector(".searchBox button")


// display all the available Employees
displayEmployees(employeesData)

// insert elem to inflate for empDet
const aside = document.querySelector(".sideDesc")
aside.insertAdjacentHTML('beforeend',
    `<button class="cancelBtn">&times;</button>
    <div class="empDet"></div>`
)
const cancelBtn = document.querySelector('.sideDesc button')
cancelBtn.addEventListener('click', removeDetails)
let sideBar = document.querySelector(".empDet")



// get all the buttons of employees and add listener to each
addBtnListener()

// add listener to search -> search employees by name
searchBtn.addEventListener('click', searchEmployee)






function displayEmployees(employees) {
    let employeeCard = employees.map((person) =>
        `<div class="card">
    <div class="imgWrap">
    <img class="img" src="${baseImgUrl}${person.pic}" alt="${person.name} img">
    </div>
    <div class="cardDet">
    <div class="pad10 hidden">
    <div><b>id: </b>${person.id}</div>
    <div><b>role: </b>${person.role}</div>
    <div><b>hobbies: </b>${person.hobbies}</div>
    <div><b>goal: </b>${person.goal}</div>
    </div>
    <button>${person.name}</button>
    </div>
    </div>`
    )
    box.innerHTML = employeeCard.join('')
}

function addBtnListener() {
    const btns = document.querySelectorAll(".cardDet button")
    btns.forEach((btn) => btn.addEventListener('click', showDetails))
}

function searchEmployee(event) {

    event.preventDefault();
    const inputElem = event.currentTarget.previousElementSibling;
    // console.log(inputElem.value)
    const filteredEmployee = employeesData.filter((elem) => elem.name.toLowerCase().includes(inputElem.value.toLowerCase()))
    displayEmployees(filteredEmployee)
    addBtnListener()
}

function showDetails(event) {

    aside.classList.remove("hidden")
    sideBar.innerHTML = "";
    box.classList.add("w80")

    sideBar.classList.remove("hidden")
    const elem = event.target.parentElement;
    let elemClone = elem.cloneNode(true);

    const img = elem.previousElementSibling
    let imgClone = img.cloneNode(true);

    elemClone.firstElementChild.classList.remove("hidden")
    sideBar.appendChild(imgClone)
    sideBar.appendChild(elemClone)

}
function removeDetails(event) {
    aside.classList.add("hidden")
    box.classList.remove("w80")

}
